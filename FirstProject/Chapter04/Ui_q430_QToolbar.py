# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/lishixi/pythonworks/FirstProject/Chapter04/q430_QToolbar.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!
import sys
from PyQt5 import QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class Ui_MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(Ui_MainWindow, self).__init__(parent)
        self.resize(800, 600)
        # menubar
        menubar=self.menuBar()
        file= menubar.addMenu("File")
        file.addAction("Show")
        file.triggered[QAction].connect(self.process_trigger)
        
        # toolbar
        tb=self.addToolBar("File")
        new=QAction(QIcon("./images/img1.jpeg"), "new", self)
        save=QAction(QIcon("./images/img1.jpeg"), "new", self)
        tb.addAction(new)
        tb.addAction(save)
        
        # status bar
        statusbar=QStatusBar()
        self.setStatusBar(statusbar)
        statusbar.showMessage("状态栏")
        
    def process_trigger(self, q):
        print(q)
        
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    ui = Ui_MainWindow()
    ui.show()
    sys.exit(app.exec_())
