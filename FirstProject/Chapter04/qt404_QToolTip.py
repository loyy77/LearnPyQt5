import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QToolTip
from PyQt5.QtGui import QFont
class WinForm(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
    def initUI(self):
        QToolTip.setFont(QFont("微软雅黑", 30))
        self.setToolTip("hello")
        self.setGeometry(200, 100, 300, 200)
        self.setWindowTitle("Test ToolTip")
    
if __name__ == "__main__":
    app=QApplication(sys.argv)
    win=WinForm()
    win.show()
    sys.exit(app.exec_())
