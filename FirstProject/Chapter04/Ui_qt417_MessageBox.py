# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/lishixi/pythonworks/FirstProject/Chapter04/qt417_MessageBox.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import *

class Ui_Form(QDialog):
    def __init__(self, parent=None):
        super( Ui_Form, self).__init__(parent)
        self.verticalLayoutWidget = QtWidgets.QWidget(Form)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(100, 10, 111, 291))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.pushButton_2 = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.openInfo)
        self.pushButton_2.setText("信息")
        self.verticalLayout.addWidget(self.pushButton_2)
        self.pushButton = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.openAbout)
        self.pushButton.setText("关于")
        self.verticalLayout.addWidget(self.pushButton)
        self.pushButton_3 = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.openQuestion)
        self.pushButton_3.setText("问题")
        self.verticalLayout.addWidget(self.pushButton_3)
        self.pushButton_4 = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_4.clicked.connect(self.openWarning)
        self.pushButton_4.setText("警告")
        self.verticalLayout.addWidget(self.pushButton_4)
        self.pushButton_5 = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_5.clicked.connect(self.openError)
        self.pushButton_5.setText("错误")
        self.verticalLayout.addWidget(self.pushButton_5)

      

    
    
    def openInfo(self):
        reply=QMessageBox.information(self, "biaoti", "you have a message", QMessageBox.Yes|QMessageBox.No, QMessageBox.Yes)
        print(reply)
    def openQuestion(self):
        QMessageBox.question(self, "biaoti", "1+1=?", QMessageBox.Yes|QMessageBox.No, QMessageBox.Yes)
    def openWarning(self):
        QMessageBox.warning(self, "biaoti", "Warning", QMessageBox.Yes, QMessageBox.Yes)
    def openError(self):
        QMessageBox.critical(self, "biaoti", "error", QMessageBox.Yes, QMessageBox.Yes)
    def openAbout(self):
        QMessageBox.about(self, "标题", "关于对话框")
    
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()

    Form.show()
    sys.exit(app.exec_())

