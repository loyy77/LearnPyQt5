import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
class Drawing(QWidget):
    def __init__(self, parent=None):
        super(Drawing, self).__init__(parent)
        self.setWindowTitle("画文字")
        self.resize(300, 300)
        self.text="欢迎学习PyQt5"
        
    def paintEvent(self, event):
        painter=QPainter(self)
        painter.begin(self)
        self.drawText(event, painter)
        painter.end()
    def drawText(self, event, qp):
        qp.setPen(QColor(168, 34, 3))
        qp.setFont(QFont("微软雅黑", 20))
        qp.drawText(event.rect(), Qt.AlignCenter, self.text)
        
if __name__ == "__main__":
    ap=QApplication(sys.argv)
    win=Drawing()
    win.show()
    sys.exit(ap.exec_())
