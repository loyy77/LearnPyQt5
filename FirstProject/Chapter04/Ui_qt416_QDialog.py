# -*- coding: utf-8 -*-


from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        Dialog.setSizeGripEnabled(True)
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(40, 30, 121, 28))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(lambda:self.openDialog(self.pushButton))
        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "QDialog例子"))
        self.pushButton.setText(_translate("Dialog", "打开一个Dialog"))
    def openDialog(self, btn):
        dialog=QDialog()
        btn=QPushButton("OK", dialog)
       
        btn.move(50, 50)
        dialog.setWindowTitle("DDD")
        dialog.setWindowModality(Qt.ApplicationModal)
        
        dialog.exec()
        

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

