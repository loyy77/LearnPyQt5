import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

if __name__ == "__main__":
    app=QApplication(sys.argv)
    win=QWidget()
    win.setGeometry(20, 20, 200, 200)
    lab1=QLabel()
    lab1.setPixmap(QPixmap("./images/img1.jpeg"))
    vbox=QVBoxLayout()
    vbox.addWidget(lab1)
    win.setLayout(vbox)
    win.setWindowTitle("QPixmap 例子")
    win.show()
    sys.exit(app.exec_())
    
