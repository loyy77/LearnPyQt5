import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
class Drawing(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
        
    def initUI(self):
        self.setGeometry(300, 300, 280, 270)
        self.setWindowTitle("钢笔样式例子")
        
    def paintEvent(self, e):
        p=QPainter()
        p.begin(self)
        self.drawLines(p)
        p.end()
    
    def drawLines(self, qp):
        pen=QPen(Qt.black, 4, Qt.SolidLine)
        
        qp.setPen(pen)
        qp.drawLine(20, 40, 250, 40)
        
        pen.setStyle(Qt.DashLine)
        qp.setPen(pen)
        qp.drawLine(20, 80, 250, 80)
        
        pen.setStyle(Qt.DotLine)
        qp.setPen(pen)
        qp.drawLine(20, 120, 250, 120)
        
        pen.setStyle(Qt.DotLine)
        qp.setPen(pen)
        qp.drawLine(20, 160, 250, 160)
        
        pen.setStyle(Qt.DashDotDotLine)
        qp.setPen(pen)
        qp.drawLine(20, 200, 250, 200)
        
        pen.setStyle(Qt.CustomDashLine)
        qp.setPen(pen)
        qp.drawLine(20, 240, 250, 240)
        
if __name__=="__main__":
    app=QApplication(sys.argv)
    win=Drawing()
    win.show()
    sys.exit(app.exec_())
    
