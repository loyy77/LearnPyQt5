import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QDesktopWidget, QPushButton, QHBoxLayout, QWidget
from PyQt5.QtGui import QIcon
class MainWindow(QMainWindow):
    def __init__(self,parent=None):
        super(MainWindow, self).__init__(parent)
        self.resize(800, 600)
        self.statusBar().showMessage("HelloPyQt5", 2000)
        self.setWindowTitle("ch04-MainWindow的例子并居中")
        self.center()
        self.button1=QPushButton("关闭主窗口")
        self.button1.clicked.connect(self.onButtonClick)
        layout=QHBoxLayout()
        layout.addWidget(self.button1)
        main_frame=QWidget()
        main_frame.setLayout(layout)
        self.setCentralWidget(main_frame)
    ## 设置窗口居中显示
    def center(self):
        screen=QDesktopWidget().screenGeometry()
        size=self.geometry()
        self.move((screen.width() - size.width())/2, (screen.height()-size.height())/2)
        
    ## 点击关闭主窗口
    def onButtonClick(self):
        sender=self.sender()
        print(sender.text()+" 被按下了")
        qApp=QApplication.instance()
        qApp.quit()
        

if __name__ =="__main__":
    app=QApplication(sys.argv)
    app.setWindowIcon(QIcon("./images/ooopic_1521883565.ico"))
    win=MainWindow()
    win.show()
    sys.exit(app.exec_())
    
        
        
