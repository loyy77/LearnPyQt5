# -*- coding: utf-8 -*-


import os, sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        Dialog.setSizeGripEnabled(True)
        self.gridLayoutWidget = QtWidgets.QWidget(Dialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 371, 111))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton_3 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.gridLayout.addWidget(self.pushButton_3, 0, 1, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 0, 0, 1, 1)
        self.pushButton_2 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.gridLayout.addWidget(self.pushButton_2, 1, 0, 1, 1)
        self.pushButton_4 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton_4.setObjectName("pushButton_4")
        self.gridLayout.addWidget(self.pushButton_4, 1, 1, 1, 1)
        self.pushButton_5 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton_5.setObjectName("pushButton_5")
        self.gridLayout.addWidget(self.pushButton_5, 0, 2, 1, 1)
        self.pushButton_6 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton_6.setObjectName("pushButton_6")
        self.gridLayout.addWidget(self.pushButton_6, 1, 2, 1, 1)
        self.txtLabel = QtWidgets.QLabel(Dialog)
        self.txtLabel.setGeometry(QtCore.QRect(20, 170, 55, 16))
        self.txtLabel.setObjectName("txtLabel")
        self.imageLabel = QtWidgets.QLabel(Dialog)
        self.imageLabel.setGeometry(QtCore.QRect(250, 170, 55, 16))
        self.imageLabel.setObjectName("imageLabel")

        self.retranslateUi(Dialog)
        self.pushButton.clicked.connect(self.copyText)
        self.pushButton_3.clicked.connect(self.pasteText)
        self.pushButton_5.clicked.connect(self.copyImage)
        self.pushButton_2.clicked.connect(self.pasteImage)
        self.pushButton_4.clicked.connect(self.copyHtml)
        self.pushButton_6.clicked.connect(self.pasteHtml)
        QtCore.QMetaObject.connectSlotsByName(Dialog)


    def copyText(self):
        clipboard=QApplication.clipboard()
        clipboard.setText("I've been clipboard!")
        pass
    def pasteText(self):
        clipboard=QApplication.clipboard()
        self.txtLabel.setText(clipboard.text())
        pass
    def copyImage(self):
        clipboard=QApplication.clipboard()
        clipboard.setPixmap(QPixmap(os.path.join(os.path.dirname(__file__), "./images/img1.jpeg")))
       
    def pasteImage(self):
        clipboard=QApplication.clipboard()
        self.imageLabel.setPixmap(clipboard.pixmap())
      
        
    def copyHtml(self):
        mimeData=QMimeData()
        mimeData.setHtml("<b>Bold and <font color=red>Red</font></b>")
        clipboard=QApplication.clipboard()
        clipboard.setMimeData(mimeData)
        pass
    def pasteHtml(self):
        clipboard=QApplication.clipboard()
        mimeData=clipboard.mimeData()
        if mimeData.hasHtml():
            self.txtLabel.setText(mimeData.html())
        pass 
        
    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.pushButton_3.setText(_translate("Dialog", "复制图像"))
        self.pushButton.setText(_translate("Dialog", "复制文本"))
        self.pushButton_2.setText(_translate("Dialog", "粘贴文本"))
        self.pushButton_4.setText(_translate("Dialog", "粘贴图像"))
        self.pushButton_5.setText(_translate("Dialog", "复制HTML"))
        self.pushButton_6.setText(_translate("Dialog", "粘贴HTML"))
        self.txtLabel.setText(_translate("Dialog", "TextLabel"))
        self.imageLabel.setText(_translate("Dialog", "TextLabel"))


if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

