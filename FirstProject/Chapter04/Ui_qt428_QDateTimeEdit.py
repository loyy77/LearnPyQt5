# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/lishixi/pythonworks/FirstProject/Chapter04/qt428_QDateTimeEdit.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(256, 156)
        Dialog.setSizeGripEnabled(True)
        self.dateTimeEdit = QtWidgets.QDateTimeEdit(Dialog)
        self.dateTimeEdit.setGeometry(QtCore.QRect(40, 40, 194, 26))
        self.dateTimeEdit.setDateTime(QtCore.QDateTime(QtCore.QDate(2018, 3, 31), QtCore.QTime(0, 0, 0)))
        self.dateTimeEdit.setMinimumDateTime(QtCore.QDateTime(QtCore.QDate(1762, 9, 14), QtCore.QTime(0, 0, 0)))
        self.dateTimeEdit.setMaximumDate(QtCore.QDate(7950, 12, 31))
        self.dateTimeEdit.setCalendarPopup(True)
        self.dateTimeEdit.setObjectName("dateTimeEdit")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(40, 80, 191, 28))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.show_date_time)
        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.dateTimeEdit.setDisplayFormat(_translate("Dialog", "yyyy/MM/dd HH:mm:ss"))
        self.pushButton.setText(_translate("Dialog", "获得日期和时间"))
    def show_date_time(self):
        print(self.dateTimeEdit.dateTime())
        print(self.dateTimeEdit.maximumDate())
        print(self.dateTimeEdit.maximumTime())
        print(self.dateTimeEdit.maximumDateTime())
        print(self.dateTimeEdit.minimumDate())
        print(self.dateTimeEdit.minimumTime())
        
        

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

