import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
class WindowDemo(QWidget):
    def __init__(self):
        super().__init__()
        label1=QLabel(self)
        label2=QLabel(self)
        label3=QLabel(self)
        label4=QLabel(self)
        
        #1 初始化标签控件
        label1.setText("这是一个文本标签")
        label1.setAutoFillBackground(True)
        ## Palette 画板、调色板
        palette=QPalette()
        palette.setColor(QPalette.Window, Qt.blue)
        label1.setPalette(palette)
        label1.setAlignment(Qt.AlignCenter)
 
        
        label2.setText("<a href='#'>欢迎使用Python GUI应用</a>")
        label3.setToolTip("这是一个图片标签")
        label3.setPixmap(QPixmap("./images/th.jpeg"))
        
        label4.setText("<a href='http://lishixi.cc'>欢迎访问</a>")
        
        label4.setAlignment(Qt.AlignRight)
        label4.setToolTip("这是一个超链接标签")
        
        
        vbox=QVBoxLayout()
        vbox.addWidget(label1)
        ## Stretch 伸展
        vbox.addStretch()
        vbox.addWidget(label2)
        vbox.addStretch()
        vbox.addWidget(label3)
        vbox.addStretch()
        vbox.addWidget(label4)
        vbox.addStretch()
        
        label1.setOpenExternalLinks(True)
        
        label4.linkActivated.connect(link_clicked)
        label4.setOpenExternalLinks(False)
        label2.linkHovered.connect(link_hover)
        
        self.setLayout(vbox)
        self.setWindowTitle("QLabel Sample")
def link_clicked(self):
    print("鼠标点击")
    
def link_hover(self):
    print("鼠标悬停")

if __name__ =="__main__":
    app=QApplication(sys.argv)
    win=WindowDemo()
    win.show()
    sys.exit(app.exec_())
