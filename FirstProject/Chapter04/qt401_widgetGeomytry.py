import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton


def on_button_click():
    print("say hi")


app = QApplication(sys.argv)
widget = QWidget()
button1 = QPushButton(widget)
button1.setText("Button1")
button1.move(50, 50)
button1.clicked.connect(on_button_click)
widget.resize(400, 300)
widget.move(400, 400)
widget.setWindowTitle("Demo Geometry")

widget.show()
print("widget:")
print(widget.height())
print("button:")
print(str(button1.x()) + "," + str(button1.y()))
print(widget.frameGeometry())
sys.exit(app.exec_())
