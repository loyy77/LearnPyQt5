import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
# 打开图片文件
# 打开文本文件装入文本区域

class Main_Form(QDialog):
    def __init__(self, parent=None):
        super(Main_Form, self).__init__(parent)
        layout=QVBoxLayout()
        self.btn=QPushButton("加载图片")
        self.btn.clicked.connect(self.btnLoadPic)
        layout.addWidget(self.btn)
        self.le=QLabel("")
        layout.addWidget(self.le)
        self.btnLoadText=QPushButton("加载文本")
        layout.addWidget(self.btnLoadText)
        self.btnLoadText.clicked.connect(self.doLoadText)
        self.contents=QTextEdit()
        layout.addWidget(self.contents)
        self.setLayout(layout)
        self.setWindowTitle("File Dialog 例子")
        
    def btnLoadPic(self):
        fname, _= QFileDialog.getOpenFileName(self,    "Open File", "/home/lishixi", "Image files(*.jpg *.gif)")
        self.le.setPixmap(QPixmap(fname))
  
        
    def doLoadText(self):
        dlg=QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setFilter(QDir.Files)
        if dlg.exec_():
            filename=dlg.selectedFiles()
            f=open(filename[0], 'r')
            with f:
                self.contents.setText(f.read())

if __name__ == "__main__":
    app=QApplication(sys.argv)
    win=Main_Form()
    win.show()
    app.exit(win.exec_())
