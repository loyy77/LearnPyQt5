# -*- coding: utf-8 -*-


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        self.pushButton = QtWidgets.QPushButton(Form)
        self.pushButton.setGeometry(QtCore.QRect(80, 40, 85, 28))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.openFontDialog)
        self.pushButton.setText("选择字体")
        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(50, 110, 271, 131))
        font = QtGui.QFont()
        font.setFamily("DejaVu Sans")
        font.setPointSize(26)
        self.label.setFont(font)
        self.label.setObjectName("label")
            
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))


    def openFontDialog(self):
        font, ok=QFontDialog.getFont()
        if ok:
            self.label.setFont(font)
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

