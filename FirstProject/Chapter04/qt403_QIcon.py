import sys
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QIcon
app=QApplication(sys.argv)
win=QWidget()
win.setGeometry(200, 200, 300, 200)
win.setWindowTitle("Icon")
win.setWindowIcon(QIcon('./images/test.ico'))
win.show()
sys.exit(app.exec_())
