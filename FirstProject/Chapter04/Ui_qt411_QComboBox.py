# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/lishixi/pythonworks/FirstProject/Chapter04/qt411_QComboBox.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!
from PyQt5.QtWidgets import *
from PyQt5 import *

class Ui_Form(QWidget):
    def __init__(self, parent=None):
        super( Ui_Form, self).__init__(parent)
        vbox = QVBoxLayout()
        self.txtItem=QLineEdit()
        
        self.resize(400, 300)
        self.comboBox = QtWidgets.QComboBox()
        self.comboBox.setGeometry(QtCore.QRect(80, 40, 141, 24))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("Java")
        self.comboBox.addItem("C")
        self.comboBox.addItem("Python")
        self.comboBox.addItem("C++")
        self.comboBox.addItem("Kotlin")
        self.comboBox.addItem("JavaScript")
        self.btn1=QPushButton("Clear")
        self.btn1.clicked.connect(self.doClear)     
        self.btn2=QPushButton("Add")
        self.btn2.clicked.connect(self.doAdd)
        self.btn3=QPushButton("Reset")
        self.btn3.clicked.connect(self.doReset)
        self.lblMsg=QLabel("")
        
        vbox.addWidget(self.txtItem) 
        vbox.addWidget(self.comboBox) 
        vbox.addWidget(self.btn1)
        vbox.addWidget(self.btn2)
        vbox.addWidget(self.btn3)
        vbox.addWidget(self.lblMsg)
        self.setLayout(vbox)
    
        self.comboBox.currentIndexChanged.connect(self.fun)

     
    def fun(self):
        # 选项的数量
        print(self.comboBox.count())
        # 当前文本
        print(self.comboBox.currentText())
        # 当前索引
        print(self.comboBox.currentIndex())
        # 获取指定索引的文本
        print(self.comboBox.itemText(2))
        # 设置指定索引的文本
        print(self.comboBox.setItemText(2, "Golang"))
        self.lblMsg.setText(self.comboBox.currentText())
        
    # 清理所有
    def doClear(self):
        self.comboBox.clear()
        
    # 添加一个
    def doAdd(self):
        self.comboBox.addItem(self.txtItem.text())
        self.txtItem.clear()
    # 重置
    def doReset(self):
        self.comboBox.addItems(["C", "Java", "Python", "Golang", "C++", "Kotlin"])
        
        
if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    ui = Ui_Form()
    ui.show()
    sys.exit(app.exec_())

