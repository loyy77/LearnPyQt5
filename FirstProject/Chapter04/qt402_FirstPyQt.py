# -*- coding:UTF-8 -*-

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QDesktopWidget
app=QApplication(sys.argv)

win=QWidget()
win.resize(300, 300)
win.setWindowTitle("Hello PyQT5，你好")
## center
screen=QDesktopWidget().screenGeometry()
size=win.size()
x=(screen.width()-size.width())/2
y=(screen.height()-size.height())/2
win.move(x, y)


win.show()
sys.exit(app.exec_())
