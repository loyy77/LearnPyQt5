# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/lishixi/pythonworks/Chapter03/ChilldrenForm2.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtWidgets

class Ui_ChildrenForm(object):
    def setupUi(self, ChildrenForm):
        ChildrenForm.setObjectName("ChildrenForm")
        ChildrenForm.resize(800, 600)
        self.centralWidget = QtWidgets.QWidget(ChildrenForm)
        self.centralWidget.setObjectName("centralWidget")
        self.textEdit = QtWidgets.QTextEdit(self.centralWidget)
        self.textEdit.setGeometry(QtCore.QRect(40, 40, 511, 381))
        self.textEdit.setObjectName("textEdit")
        ##ChildrenForm.setCentralWidget(self.centralWidget)

        self.retranslateUi(ChildrenForm)
        QtCore.QMetaObject.connectSlotsByName(ChildrenForm)

    def retranslateUi(self, ChildrenForm):
        _translate = QtCore.QCoreApplication.translate
        ChildrenForm.setWindowTitle(_translate("ChildrenForm", "MainWindow"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ChildrenForm = QtWidgets.QMainWindow()
    ui = Ui_ChildrenForm()
    ui.setupUi(ChildrenForm)
    ChildrenForm.show()
    sys.exit(app.exec_())

