import sys
from PyQt5.QtWidgets  import QApplication, QMainWindow, QFileDialog
## 从UI_MainForm.py 导入类Ui_MainWindow
from Ui_MainForm import Ui_MainWindow

##继承QMainWindow和Ui_MainWindow
class MainForm(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainForm, self).__init__()
        self.setupUi(self)
        self.fileCloseAction.triggered.connect(self.close)
        self.fileOpenAction.triggered.connect(self.openMsg)
        
    def openMsg(self):
        file, filetype=QFileDialog.getOpenFileName(self, "打开", "/home/lishixi", "所有文件(*);;文本文件(*.txt);;数据文件(*.dat)")
        print("filetype="+filetype)
        print("file="+file)
        self.statusbar.showMessage(file)
        
if __name__ =="__main__":
    app=QApplication(sys.argv)
    win=MainForm()
    win.show()
    sys.exit(app.exec_())
