# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/lishixi/pythonworks/Chapter03/ChilldrenForm2.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Ui_ChildrenForm(object):
    def setupUi(self, Ui_ChildrenForm):
        Ui_ChildrenForm.setObjectName("Ui_ChildrenForm")
        Ui_ChildrenForm.resize(528, 401)
        self.centralWidget = QtWidgets.QWidget(Ui_ChildrenForm)
        self.centralWidget.setObjectName("centralWidget")
        self.textEdit = QtWidgets.QTextEdit(self.centralWidget)
        self.textEdit.setGeometry(QtCore.QRect(0, 0, 521, 401))
        self.textEdit.setObjectName("textEdit")
        Ui_ChildrenForm.setCentralWidget(self.centralWidget)

        self.retranslateUi(Ui_ChildrenForm)
        QtCore.QMetaObject.connectSlotsByName(Ui_ChildrenForm)

    def retranslateUi(self, Ui_ChildrenForm):
        _translate = QtCore.QCoreApplication.translate
        Ui_ChildrenForm.setWindowTitle(_translate("Ui_ChildrenForm", "MainWindow"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Ui_ChildrenForm = QtWidgets.QMainWindow()
    ui = Ui_Ui_ChildrenForm()
    ui.setupUi(Ui_ChildrenForm)
    Ui_ChildrenForm.show()
    sys.exit(app.exec_())

